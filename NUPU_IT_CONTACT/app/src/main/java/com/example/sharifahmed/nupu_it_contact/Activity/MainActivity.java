package com.example.sharifahmed.nupu_it_contact.Activity;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.sharifahmed.nupu_it_contact.Adapter.RecyclerAdapter;
import com.example.sharifahmed.nupu_it_contact.R;


import com.example.sharifahmed.nupu_it_contact.Database.ContactDatabaseSource;
import com.example.sharifahmed.nupu_it_contact.Model.ContactModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {



    ContactModel contactModel;
    ArrayList<ContactModel>contactModels;
    ContactDatabaseSource contactDatabaseSource;



    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerAdapter;
    private RecyclerView.LayoutManager layoutManager;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewId);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        contactModels = new ArrayList<>();

        contactDatabaseSource = new ContactDatabaseSource(this);

        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,null,null, null);
        while (phones.moveToNext())
        {
            //Read Contact Name
            String name=phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

            //Read Phone Number
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            if(name!=null && phoneNumber !=null)
            {
                contactModel = new ContactModel(name,phoneNumber);
                contactDatabaseSource.addContact(contactModel);
            }
        }
        phones.close();
        int totalContact = contactDatabaseSource.getAllContact().size();
        if(totalContact != 0){
            recyclerAdapter = new RecyclerAdapter(contactDatabaseSource.getAllContact());
            recyclerView.setAdapter(recyclerAdapter);
        }
        else
            Toast.makeText(MainActivity.this, "There Is No Contact", Toast.LENGTH_SHORT).show();

    }
    public void loadMoreContact(View view) {

    }

}
