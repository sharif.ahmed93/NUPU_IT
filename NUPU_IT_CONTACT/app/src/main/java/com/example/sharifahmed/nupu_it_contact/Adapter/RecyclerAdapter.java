package com.example.sharifahmed.nupu_it_contact.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sharifahmed.nupu_it_contact.Model.ContactModel;
import com.example.sharifahmed.nupu_it_contact.R;

import java.util.ArrayList;

/**
 * Created by SHARIF AHMED on 3/31/2017.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {

    ArrayList<ContactModel> contactModelArrayList;

    public RecyclerAdapter(ArrayList<ContactModel> contactModelArrayList) {

        this.contactModelArrayList=contactModelArrayList;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_layout,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(view);


        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        holder.nameText.setText(contactModelArrayList.get(position).getName());
        holder.numberText.setText(contactModelArrayList.get(position).getPhoneNumber());

    }

    @Override
    public int getItemCount() {
        return contactModelArrayList.size();
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder
    {

        TextView nameText;
        TextView numberText;
        public RecyclerViewHolder(View itemView) {
            super(itemView);

            nameText = (TextView) itemView.findViewById(R.id.contactNameId);
            numberText = (TextView) itemView.findViewById(R.id.contactNumberId);
        }
    }
}
