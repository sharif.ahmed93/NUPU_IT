package com.example.sharifahmed.nupu_it_contact.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


import com.example.sharifahmed.nupu_it_contact.Model.ContactModel;

import java.util.ArrayList;

public class ContactDatabaseSource {
    DatabaseHelper databaseHelper;
    SQLiteDatabase database;
    ContactModel contactModel;

    public ContactDatabaseSource(Context context) {
        databaseHelper=new DatabaseHelper(context);
    }


    public void open(){
        database=databaseHelper.getWritableDatabase();
    }
    public void close(){
        databaseHelper.close();
    }

    public boolean addContact(ContactModel contactModel){
        this.open();

        ContentValues contentValues=new ContentValues();
        contentValues.put(DatabaseHelper.COL_NAME,contactModel.getName());
        contentValues.put(DatabaseHelper.COL_PHONE,contactModel.getPhoneNumber());


        long inserted=database.insert(DatabaseHelper.TABLE_CONTACT,null,contentValues);

        this.close();
        if(inserted>0){
            return true;
        }else {
            return false;
        }

    }

    public ArrayList<ContactModel> getAllContact(){
        ArrayList<ContactModel> contactModels=new ArrayList<>();
        this.open();

        Cursor cursor=database.rawQuery("select * from "+DatabaseHelper.TABLE_CONTACT + " limit 10",null);

        if(cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
            for(int i=0;i<cursor.getCount();i++){
                int mId=cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COL_ID));
                String mName=cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_NAME));
                String mPhone=cursor.getString(cursor.getColumnIndex(DatabaseHelper.COL_PHONE));
                contactModel=new ContactModel(mId,mName,mPhone);
                cursor.moveToNext();
                contactModels.add(contactModel);
            }
        }
        cursor.close();
        this.close();
        return contactModels;
    }

}
